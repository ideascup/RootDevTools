/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.handlerChangeNetworkState();
        document.addEventListener('online', this.handlerChangeNetworkState);
        document.addEventListener('offline', this.handlerChangeNetworkState);

        var portInput = document.querySelector('.tcp-port');
        var tcpCheckbox = document.querySelector('.toggle-tcp');
        var tcpPort = localStorage.getItem('tcp-port');
        if (tcpPort) {
            portInput.value = tcpPort;
        }
        var isEnableTCP = localStorage.getItem('tcp') === 'true';
        tcpCheckbox.checked = isEnableTCP;

        tcpCheckbox.addEventListener('change', function (e) {
            var port = tcpCheckbox.checked ? 
                -1 
                : /\d+/.test(portInput.value) ? 
                    parseInt(portInput.value) 
                    : 5555;
            var command = 'setprop service.adb.tcp.port ' + port + ' && stop adbd && start adbd';

            root.run(command, function (res) {
                localStorage.setItem('tcp', (port > -1) + '');
            }, function () {
                alert('An error occoured while executing the command.');
            });
        });

        portInput.addEventListener('input', function (e) {
            if (portInput.value && /\d+/.test(portInput.value)) {
                localStorage.setItem('tcp-port', portInput.value);
            }
        })
    },

    handlerChangeNetworkState: function () {
        var ipText = document.getElementById('ip-local');
        networkinterface.getWiFiIPAddress(function (ip) {
            ipText.innerText = ip;
        }, function (err) {
            ipText.innerText = '...';
        });
    }
};

app.initialize();